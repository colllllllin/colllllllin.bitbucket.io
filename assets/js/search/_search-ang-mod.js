angular.module('adg-search', []);

angular.module('adg-search').controller("adg-search-controller", function(){
    var vm = this;
    vm.filterText = null;
    vm.filterSelect = null;

    vm.assetResults = [
           {
               imgPath:'../assets/img/iphone_img2.png',
               manufacturer: 'Apple',
               model:'iPhone 6',
               capacity:'64GB',
               color:'Black'
           },
           {
               imgPath:'../assets/img/iphone_img2.png',
               manufacturer: 'Apple',
               model:'iPhone 6',
               capacity:'64GB',
               color:'Silver'
           },
           {
               imgPath:'../assets/img/iphone_img2.png',
               manufacturer: 'Apple',
               model:'iPhone 6',
               capacity:'32GB',
               color:'Black'
           },
           {
               imgPath:'../assets/img/iphone_img2.png',
               manufacturer: 'Apple',
               model:'iPhone 6',
               capacity:'32GB',
               color:'Silver'
           },
           {
               imgPath:'../assets/img/iphone_img2.png',
               manufacturer: 'Apple',
               model:'iPhone 6',
               capacity:'128GB',
               color:'Black'
           },
           {
               imgPath:'../assets/img/iphone_img2.png',
               manufacturer: 'Apple',
               model:'iPhone 6',
               capacity:'128GB',
               color:'Silver'
           },
           {
               imgPath:'../assets/img/iphone_img2.png',
               manufacturer: 'Apple',
               model:'iPhone 6s',
               capacity:'64GB',
               color:'Black'
           },
           {
               imgPath:'../assets/img/iphone_img2.png',
               manufacturer: 'Apple',
               model:'iPhone 6s',
               capacity:'64GB',
               color:'Silver'
           },
           {
               imgPath:'../assets/img/iphone_img2.png',
               manufacturer: 'Apple',
               model:'iPhone 6s',
               capacity:'32GB',
               color:'Black'
           },
           {
               imgPath:'../assets/img/iphone_img2.png',
               manufacturer: 'Apple',
               model:'iPhone 6s',
               capacity:'32GB',
               color:'Silver'
           },
           {
               imgPath:'../assets/img/iphone_img2.png',
               manufacturer: 'Apple',
               model:'iPhone 6s',
               capacity:'128GB',
               color:'Black'
           },
           {
               imgPath:'../assets/img/iphone_img2.png',
               manufacturer: 'Apple',
               model:'iPhone 6s',
               capacity:'128GB',
               color:'Silver'
           },
           {
               imgPath:'../assets/img/iphone_img2.png',
               manufacturer: 'Apple',
               model:'iPhone 5s',
               capacity:'64GB',
               color:'Silver'
           },
           {
               imgPath:'../assets/img/iphone_img2.png',
               manufacturer: 'Apple',
               model:'iPhone 5s',
               capacity:'32GB',
               color:'Black'
           },
           {
               imgPath:'../assets/img/iphone_img2.png',
               manufacturer: 'Apple',
               model:'iPhone 5s',
               capacity:'32GB',
               color:'Silver'
           },
           {
               imgPath:'../assets/img/iphone_img2.png',
               manufacturer: 'Apple',
               model:'iPhone 5s',
               capacity:'128GB',
               color:'Black'
           },
           {
               imgPath:'../assets/img/iphone_img2.png',
               manufacturer: 'Apple',
               model:'iPhone 5s',
               capacity:'128GB',
               color:'Silver'
           },

       ];
});

